import argparse
import logging

from google.cloud import aiplatform
from datetime import datetime

logger = logging.getLogger(__name__)


class ArgParser(object):
    """
    Argument parser for torch-model-archiver commands
    """

    @staticmethod
    def export_model_args_parser():
        """Argument parser for torch-model-export"""

        parser_export = argparse.ArgumentParser(
            prog="upload_and_deploy_model",
            description="Upload and deploy model to Vertex AI",
            formatter_class=argparse.RawTextHelpFormatter,
        )

        parser_export.add_argument(
            "--prediction-image-uri",
            required=True,
            type=str,
            help="URI of the prediction image.",
        )

        parser_export.add_argument(
            "--bucket-name",
            required=False,
            type=str,
            default="picnic-jupyter-datasets-dev-europe-west3",
            help="Bucket name, to upload model artifacts to. \n"
                 "Default: 'picnic-jupyter-datasets-dev-europe-west3'",
        )

        parser_export.add_argument(
            "--app-name",
            required=False,
            type=str,
            default="text-pod",
            help="Name of the app. \n"
                 "Default: 'text-pod'",
        )

        parser_export.add_argument(
            "--project-id",
            required=False,
            type=str,
            default="picnic-backend-356310",
            help="Project ID. \n"
                 "Default: 'picnic-backend-356310'",
        )

        parser_export.add_argument(
            "--region",
            required=False,
            type=str,
            default="europe-west4",
            help="Region. \n"
                 "Default: 'europe-west1'",
        )

        parser_export.add_argument(
            "--accelerator",
            required=False,
            type=str,
            default="NVIDIA_TESLA_T4",
            help="Accelerator type. \n"
                 "Default: 'NVIDIA_TESLA_T4'",
        )

        parser_export.add_argument(
            "--machine-type",
            required=False,
            type=str,
            default="n1-standard-64",
            help="Machine type. \n"
                 "Default: 'n1-standard-64'",
        )

        parser_export.add_argument(
            "--accelerator-count",
            required=False,
            type=int,
            default=4,
            help="Accelerator count. \n"
                 "Default: 4",
        )

        return parser_export


def upload_to_model_registry(args, aip: aiplatform) -> aiplatform.Model:
    """ Upload model to model registry."""

    models = aiplatform.Model.list()
    for model in models:
        if f"{args.app_name}-model" == model.display_name:
            logging.info("Model already exists in model registry.")
            date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            model.delete(sync=True)
            logging.info(f"Model deleted at {date}")
            model_upload = aip.Model.upload(
                display_name=f"{args.app_name}-model",
                description=f"{args.app_name} container",
                serving_container_image_uri=args.prediction_image_uri,
            )
            break

    else:
        logging.info("Uploading model to model registry...")

        model_upload = aip.Model.upload(
            display_name=f"{args.app_name}-model",
            description=f"{args.app_name} container",
            serving_container_image_uri=args.prediction_image_uri,
        )

    model_upload.wait()

    logging.info("Model uploaded to model registry.")
    logging.info(model_upload.display_name)
    logging.info(model_upload.resource_name)

    return model_upload


def create_endpoint(args, aip: aiplatform) -> aiplatform.Endpoint:
    """Create endpoint."""

    for e in aiplatform.Endpoint.list():
        if e.display_name == f"{args.app_name}-endpoint":
            logging.info("Endpoint already exists.")
            endpoint = e
            break
    else:
        logging.info("Creating endpoint...")
        endpoint = aip.Endpoint.create(display_name=f"{args.app_name}-endpoint")

    logging.info(endpoint.display_name)
    logging.info(endpoint.resource_name)
    logging.info(endpoint.create_time)

    logging.info("Endpoint created.")
    return endpoint


def deploy_model_to_endpoint(args, model: aiplatform.Model, endpoint: aiplatform.Endpoint) -> None:
    """Deploy model to endpoint."""

    logging.info("Deploying model to endpoint...")
    deployed_model = model.deploy(
        endpoint=endpoint,
        deployed_model_display_name=f"{args.app_name}-model",
        machine_type=f"{args.machine_type}",
        accelerator_type=f"{args.accelerator}",
        accelerator_count=args.accelerator_count,
        traffic_percentage=100,
        deploy_request_timeout=2500,
        sync=True,
    )

    deployed_model.wait()

    logging.info(deployed_model.display_name)
    logging.info(deployed_model.resource_name)
    logging.info(deployed_model.create_time)

    logging.info("Model deployed to endpoint.")


def main():
    args = ArgParser.export_model_args_parser().parse_args()
    aiplatform.init(
        project=args.project_id,
        location=args.region,
        staging_bucket=args.bucket_name,
    )
    endpoint = create_endpoint(args, aiplatform)
    model = upload_to_model_registry(args, aiplatform)
    deploy_model_to_endpoint(args, model, endpoint)


if __name__ == "__main__":
    main()
