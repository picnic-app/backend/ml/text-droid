# text-droid

This repository contains the code for the text-droid model.
The model is one of text generator from huggingface transformers.

## How to use

Repository contains two main components:

- `app` - handler for fast api server for text model with inference method
- `model_upload_and_deploy.py` - cli tool for model upload and deploy

## How to deploy

1. Install requirements from `requirements.txt`
2. Run `docker build -t custom-chat-bot:<version> .`
3. Run `docker tag custom-chat-bot:<version> <artifacts_uri>/vertex/custom-chat-bot:<version>`
4. Run `docker push <artifacts_uri>/vertex/custom-chat-bot:<version>`
5. Run `model_upload_and_deploy.py --pytorch_prediction_image_uri <artifacts_uri>/vertex/custom-chat-bot:<version>` with required arguments:

```
usage: upload_and_deploy_model [-h] [--bucket-name BUCKET_NAME] --prediction-image-uri PREDICTION_IMAGE_URI [--app-name APP_NAME] [--project-id PROJECT_ID] [--region REGION]

Upload and deploy model to Vertex AI

options:
  -h, --help            show this help message and exit
  --bucket-name BUCKET_NAME
                        Bucket name, to upload model artifacts to.
                        Default: 'picnic-jupyter-datasets-dev-europe-west3'
  --prediction-image-uri PREDICTION_IMAGE_URI
                        URI of the prediction image.
  --app-name APP_NAME   Name of the app.
                        Default: 'text-pod'
  --project-id PROJECT_ID
                        Project ID.
                        Default: 'picnic-backend-356310'
  --region REGION       Region.
                        Default: 'europe-west4'

```

## Contract

For now very simple request:
```json
{
"instances": [
   {"message": "text"}
]
}
```

where `text` needs to have structure:
```
[CHARACTER]'s Persona: [A few sentences about the character you want the model to play]
<START>
[OPTIONAL: SHORT SUMMARY ABOUT CONVERSATION]
[DIALOGUE HISTORY]
You: [Your input message here]
[CHARACTER]:
```

Response:
```json
{
 "predictions": [
   "response_text"
 ],
 "deployedModelId": "vertex_ai_model_id",
 "model": "endpoint_location_vertex_ai",
 "modelDisplayName": "model_display_name",
 "modelVersionId": "model_version"
}
```