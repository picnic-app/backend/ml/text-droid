FROM europe-docker.pkg.dev/vertex-ai/prediction/pytorch-gpu.1-13:latest

WORKDIR /app

USER root
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY /app /app

EXPOSE 8080
ENTRYPOINT ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080"]
