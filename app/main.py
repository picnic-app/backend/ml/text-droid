import os
from fastapi import FastAPI, Request
from starlette.responses import JSONResponse
from server import Handler

app = FastAPI(title="chatbot")
model = Handler()


# Health route
@app.get(os.environ['AIP_HEALTH_ROUTE'], status_code=200)
def health():
    return {"status": "healthy"}


# Predict route
@app.post(os.environ['AIP_PREDICT_ROUTE'])
async def predict(request: Request):
    body = await request.json()

    instances = body["instances"]

    preds = model.inference(instances)

    formatted_predictions = {
        "predictions": []
    }
    for i in preds:
        formatted_predictions["predictions"].append(i)
    return JSONResponse(formatted_predictions)
