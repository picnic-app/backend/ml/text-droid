import logging

import torch
from torch.nn.parallel import DistributedDataParallel
from transformers import AutoTokenizer, AutoModelForCausalLM
import torch.distributed as dist

logger = logging.getLogger(__name__)
model_id = "PygmalionAI/pygmalion-6b"


class Handler:

    def __init__(self):
        """Initializes the pipe."""

        self.device = 'cuda'

        self.tokenizer = AutoTokenizer.from_pretrained(model_id)

        model = AutoModelForCausalLM.from_pretrained(model_id)
        model.to(self.device)

        num_gpus = torch.cuda.device_count()

        # Move the model to the available GPUs
        if num_gpus > 1:
            # Initialize the distributed environment
            dist.init_process_group(backend='nccl')  # Use 'nccl' for GPU communication
            # If multiple GPUs are available, wrap the model with DataParallel
            model = DistributedDataParallel(model)

        model.eval()
        self.pipe = model

    def inference(self, preprocessed_data):
        """Run the inference."""
        answers = []
        for pd in preprocessed_data:
            encoded_input = self.tokenizer(pd["message"], return_tensors='pt').to(self.device)
            outputs = self.pipe.generate(**encoded_input, max_new_tokens=256, do_sample=True, top_k=50, top_p=0.95,
                                         temperature=0.25)
            new = self.tokenizer.batch_decode(outputs, skip_special_tokens=True)[0]
            new = new.replace(pd["message"], '')
            answers.append(new[:new.find('\nYou:')].strip())
        return answers
